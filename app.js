var ville = prompt("Ville ?");

$.getJSON("http://api.openweathermap.org/data/2.5/weather?q="+ ville +"&units=metric&appid=09b6c7d20d11e9282060848e598ebab8", function(data) {
    console.log(data);

    // Récupération des données de l'api :
    var temp = data.main.temp;
    console.log(temp + "°C");

    var name = data.name;
    var lat = data.coord.lat;
    console.log("lat : " + lat);
    var lon = data.coord.lon;
    console.log("lon : " + lon);

    // Ajout à la page html :

    // Heure :
    $("<h2>La météo de "+ name +" aujourd'hui à " + moment().format('LTS') + ".</h2>").appendTo("#container");

    // Température :
    $("<p>Il fait " + temp + "°C.</p>").appendTo("#container");

    // Coordonnées :
    $("<p>La latitude est de " + lat + "° et la longitude de " + lon + "°.</p>").appendTo("#container");

    


})

$.getJSON("https://master.apis.dev.openstreetmap.org/",function(data) {
    console.log(data);
})