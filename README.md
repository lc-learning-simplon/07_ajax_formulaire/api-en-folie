# API EN FOLIE

## Objectif
- Manipuler le DOM.
- Utiliser la librairie de JQuery
- Utiliser de l'AJAX
- Utiliser une API

## Contexte
Vous avez des donnée dans un format [JSON](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON). Vous allez devoir récupérer ces données et afficher les dans votre HTML.

## Intention
- Manipuler des données
- Faire des requêtes asynchrone
- Requête vers une API

## Compétence.s concernée.s
- Réaliser une interface utlisateur web
- Dévellopper un interface utilisateur dynamique

## Critères de réussite
- Le code est **indenté** et **lisible**.
- Il n’y a pas d’erreurs dans le code.
- Le rendu est similaire à celui du wireframe, sur plusieurs navigateurs et mobile.

## Livrable.s
> Dimanche 23h59
- **Lien vers le repo gitlab**.
- **Lien vers la version en ligne**.

## Réalisation attendues

#### Les API's
- [Météo API](#openweathermap)
- [Stars Wars API](#swapi)
- [Overwatch API](#owapi)
- [RIOT API](#riotapi)
- [Movie API](#omdbapi)
- [Football API](#apifootball)

> Choississez une API dans celles cité ci-dessus.
> Avant tout choses ! Faite un `npm install`

-----------------------------------------

#### OPENWEATHERMAP

Avec l'API de [Openweathermap](https://openweathermap.org/), vous allez devoir :
- Afficher la météo de Toulouse (température en °C et description)
- Récupérer la latitude et la longitude de Carbonne, utiliser une requête vers GoogleMaps Api / OpenstreetMap (pas la peine de clé API si vous ne faites pas bcp de requêtes)
- Récupérer le temps et la distance d'un itinéraire de Toulouse à Carbonne. Utiliser une requête vers GoogleMaps API.
Et pour finir l'utilisateur saisit une ville pour obtenir :    
* la météo
* l'heure
* la latitude et la longitude
* le temps et la distance depuis Carbonne.

et les mettres en formes sur votre fichier `index.html`

-------------------------------------------------------

#### SWAPI

Avec SWAPI de [Stars Wars API](https://www.swapi.co/), vous allez devoir :
- Faire une fiche pour lister :
    + Toutes les planètes
    + Tous les Personnages
    + Tous les Films
- Pour chaques Planètes afficher toutes les informations ainsi que les `résidents` et les `films` ou elles apparaissent.
- Pour chaques Personnages afficher toutes les informations ainsi que les films, planètes ou ils apparaissent, son espèce, ses véhicules et ses vaisseaux.
- Pour Chaques films afficher toutes les informations ainsi que les personnages, planètes, véhicules, espèces, vaiseaux qui apparait dans ce film.
- Créer un formulaire qui va afficher des information en  fonction du nom de personnage, planète, film demander.

-------------------------------------------------------

#### OWAPI
#### RIOTAPI

Pour Overwatch ou Riot API, vous allez devoir :
- Récupérer votre profil et afficher toutes vos states : 
    + Partie Jouer
    + Classement
    + Défaites
    + Victoire
    + ...
- Créer un formulaire qui va rechercher le profile d'une personne pour afficher ses stats.

-------------------------------------------------------

#### OMDBAPI

Pour le [Movie API](https://www.omdbapi.com/), vous allez devoir : 
- Récupérer toutes les informations du film `Kung Fury`
    + Titre
    + Date de sortie
    + Genre
    + Acteurs
    + ....
- Créer un formulaire qui va rechercher un film en fonction du titre entrer. 

-------------------------------------------------------

#### APIFOOTBALL

Pour le [Football API](https://apifootball.com/), vous allez devoir :
- Récupérer toutes les informations des équipes :
    + pour chaque équipe afficher :
        * son Nom
        * son Badge
        * tous les Joueurs
        * le nom du coach
- Récupérer toutes les information de chaques joueurs.
- Récuperer les prédictions pour les matchs. 
- Faire un formulaire qui va chercher les informations d'un joueurs ou d'une équipes.

**Bonus** :
- Pour les plus téméraire d'entre vous, remplacer tout l'ajax de jQuery par [fetch](https://developer.mozilla.org/fr/docs/Web/API/Fetch_API/Using_Fetch)
- Utiliser [leaflet](https://leafletjs.com/) pour poser un point directement sur la carte.
**Bonus ultime : affichez l'heure**

**Contrainte** :
- Pour insérer les données dans l'`index.html` vous allez devoir utiliser [mustach](https://github.com/janl/mustache.js/)
- Dans votre `index.html`, vous devez avoir juste une `div#app`
